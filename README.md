# SpringJPADemo

Books - Table
id
title

JPA CRUD example
REST-API

Methods	Urls	Actions
POST	/api/books	            create new Book
GET	    /api/books	            retrieve all Books
GET	    /api/books?title=xy	    retrieve all Books by Title
GET	    /api/books/:id	        retrieve a Book by :id
PUT	    /api/books/:id	        update a Book by :id
DELETE	/api/books/:id	        delete a Book by :id

localhost:8080/api/books?title=insel
